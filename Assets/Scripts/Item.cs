﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public int itemID;
    public string itemName;
    public string itemDesc;
    public Texture2D icon;
    public ItemType itemType;
    public int itemWeight;

    public enum ItemType {Weapon, Armor, Consumable, Quest, Misc};

    public Item(){
        // empty item.
    }
}
