﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : Item
{
    public DamageType itemDamageType;
    public Dice itemDice;
    public int itemModifier;
    public string itemClasses;

    public enum DamageType {Slashing, Piercing, Bludgeoning, Fire, Cold, Poison, Acid, Psychic, Necrotic, Radiant, Lightning, Thunder, Force};
    public enum Dice {d4,d6,d8,d12,d20};

    public Weapon (int ID, string name, string desc, ItemType type, int weight, DamageType damageType, Dice dice, int modifier, string classes){
        itemID = ID;
        itemName = name;
        itemDesc = desc;
        itemType = type;
        itemWeight = weight;
        itemDamageType = damageType;
        itemDice = dice;
        itemModifier = modifier;
        itemClasses = classes;
    }

}
