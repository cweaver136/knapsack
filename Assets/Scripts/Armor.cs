﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armor : Item
{
    public WeightClass itemWeightClass;
    public bool isStealthy;
    public int itemAC;
    public string itemClasses;

    public enum WeightClass {Light, Medium, Heavy};

    public Weapon (int ID, string name, string desc, ItemType type, int weight, WeightClass wc, bool stealth, int ac, string classes){
        itemID = ID;
        itemName = name;
        itemDesc = desc;
        itemType = type;
        itemWeight = weight;
        itemWeightClass = wc;
        isStealthy = stealth;
        itemAC = ac;
        itemClasses = classes;
    }

}
