﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
   public GameObject inventory;
   private int allSlots;
   private GameObject[] slot;

   public GameObject slotHolder;

   void Start(){
       allSlots = 24;
       slot = new GameObject[allSlots];

       for (int i = 0; i < allSlots; i++){
           slot[i] = slotHolder.transform.GetChild[i].gameObject;
       }
   }
}